import java.io.*;
import java.net.*;
import java.util.Scanner;


public class UDPClient {

	public static void main(String[] args) throws IOException{
		// Erzeugt direkt ein gebundenes Socket, nutzt dazu einen zugewiesenen (High)-Port
		DatagramSocket socket = new DatagramSocket();

		// Es gibt keinen Verbindungsaufbau, sondern es folgt sofort das Anwendungsprotokoll
		// In unserem Fall, wartet der Server auf eine Nachricht von einem beliebigen (Peer o. Client)
		byte[] buffer = new byte[1024];
		DatagramPacket datagram = new DatagramPacket(buffer, buffer.length, new InetSocketAddress("localhost", 2020));

		double op1 = 0, op2 = 0;
		char op = '\0';

		boolean reading = true;
		while(reading) try {
			System.out.println("Bitte gib eine Aufgabe der Form: operand1 operator operand2 ein.");
			
			// Empfange Daten von Butzer
			Scanner scanner = new Scanner(System.in);

			op1 = scanner.nextDouble();

			op = scanner.next().charAt(0);

			op2 = scanner.nextDouble();

			reading = false;

		} catch(Exception ignored) {

		}

		ByteArrayOutputStream aufgabe = new ByteArrayOutputStream(17);
		DataOutputStream dataOut = new DataOutputStream(aufgabe);
		dataOut.writeDouble(op1);
		dataOut.writeChar(op);
		dataOut.writeDouble(op2);
		dataOut.close();

		datagram.setData(aufgabe.toByteArray());

		// Sende die Aufgabe an den Server
		socket.send(datagram);

		// gib dem Datagramm für zu empfangende Daten den Puffer (s.o.) zurück
		datagram.setData(buffer);

		//Empfange Ergebnis vom Server
		socket.receive(datagram);
		
		DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(datagram.getData()));

		System.out.println(dataIn.readDouble());

		dataIn.close();
	}
}