import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class TCPClient {
    public static void main(String[] args) throws IOException {
        // Der TCP-Client benötigt zum Verbindungsaufbau die IP-Adresse des Servers und dessen Port-Nummer
        Socket socket = new Socket("127.0.0.1", 20200);

        // Führe Verbindungsaufbau mit Ziel-IP und Port durch
//        socket.connect(new InetSocketAddress("127.0.0.1", 20200));

        // Leseseite
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        // Schreibseite als PrintWriter, wegen AutoFlush!!
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

        Scanner scanner = new Scanner(System.in);

        // Anwendungsprotokoll
        String line;
        do {
            // Warte auf erstes Paket (Hello) vom Server
            System.out.println(reader.readLine());

            line = scanner.nextLine();
            writer.println(line);
        } while (!line.toLowerCase().trim().equals("quit"));

        System.out.println(reader.readLine());

        //Am Ende schließe die gesamte Verbindung führt bei TCP ein Reset durch, der java Server erhält eine Exception!
        socket.close();
    }
}
