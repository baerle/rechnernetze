import java.io.*;
import java.net.*;
import java.util.Arrays;


public class UDPServer {

	public static void main(String[] args) throws IOException{
		// Erzeugt direkt ein gebundenes Socket
		DatagramSocket socket = new DatagramSocket(2020);

		// Es gibt keinen Verbindungsaufbau, sondern es folgt sofort das Anwendungsprotokoll
		// In unserem Fall, wartet der Server auf eine Nachricht von einem beliebigen (Peer o. Client)
		byte[] buffer = new byte[1024];
		DatagramPacket datagram = new DatagramPacket(buffer, buffer.length);

		do {
			datagram.setData(buffer);
			socket.receive(datagram);

			// Können ein empfangenes Paket direkt als Antwortpaket wiederverwenden
			// es geht automatishc an die Absenderadresse zurück!
			System.out.println(datagram.getAddress());

			// Protokollfehler, nur Pakete der Größe 18 Byte erlaubt
			if (datagram.getLength() != 18) {
				System.out.println(datagram.getLength() + ": " + Arrays.toString(datagram.getData()));
				datagram.setData("Nur 18-Byte Pakete erlaubt\r\n".getBytes());
			} else {
				// Entnehme die Daten aus dem Paket, berechne das Ergebnis und sende es zurück
				DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(buffer));

				double op1 = dataIn.readDouble();
				char op = dataIn.readChar();
				double op2 = dataIn.readDouble();

				double res = calculate(op1, op2, op);
				ByteArrayOutputStream ergebnis = new ByteArrayOutputStream(8);
				DataOutputStream dataOut = new DataOutputStream(ergebnis);
				dataOut.writeDouble(res);
				dataOut.close();

				// setze Antwort als Bytefolge in das Datagramm
				datagram.setData(ergebnis.toByteArray());
			}

			socket.send(datagram);

		} while (true);
	}

	public static double calculate(double op1, double op2, char op) {
		double result = Double.NaN;
		
		switch (op) {
			case '*' -> result = op1 *  op2;
			case '/', ':' -> result = op1 / op2;
			case '+' -> result = op1 + op2;
			case '-' -> result = op1 - op2;
		}

		return result;
	}
}