import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class TCPServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(20200);
        // Das binden des Socket an einen TCP-Port, wird in obrigem Konstruktor autom. ausgeführt!
        // serverSocket.bind(new InetSocketAddress(2020));

        do {
            // Lassen Server nun auf Clients warten und akzeptieren den Verbindungsaufbau
            Socket socket = serverSocket.accept();

            new Thread(() -> {
                try {
                    // Abfrage der TCP-Puffer
                    // Schreibpuffer
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

                    // Lesepuffer
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    // Anwendungsprotokoll
                    // Server sendet Hello an Client
                    writer.println("Hello - MyServer v1.0");
                    // Datenmenge reicht nicht für die MSS; also muss TCP die Daten - Nicht benötigt durch autoflush
                    // writer.flush();

                    boolean running = true;
                    while (running) try {
                        String line = reader.readLine();
                        if (line == null || line.toLowerCase().equals("quit")) {
                            running = false;
                            line = "Goodbye.";
                        }
                        writer.println(line);
                    } catch (SocketException e) {
                        System.err.println(e);
                        break;
                    }

                    System.out.println(socket);

                    // Schließe die Verbindung zum Client
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        } while (true);
    }
}
